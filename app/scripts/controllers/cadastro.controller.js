(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCriar.controllers')
		.controller('cadastroCtrl', cadastroCtrl);

	cadastroCtrl.$inject = ['$api', '$state'];

	function cadastroCtrl($api, $state) {

		var vm = this;
		vm.formcadastro = {};

		vm.sendCadastre = sendCadastre;

		///////////////////////


		if (window.StatusBar) StatusBar.styleLightContent();

		function sendCadastre() {
			$api.sendCadastre(vm.formcadastro).then(function(response){
				console.log(response);
				navigator.notification.alert(
					'Você foi cadastrado com sucesso.',  // message
					null,         	// callback
					'Obrigado',    	// title
					'OK'          	// buttonName
				);

				$state.go($state.current, {}, {reload: true});

			}, function(error){
				console.log(error);
				navigator.notification.alert(
					'Erro ao tentar se cadastrar.',  // message
					null,         	// callback
					'Atenção',    	// title
					'OK'          	// buttonName
				);
			});
		}

	}

})();
