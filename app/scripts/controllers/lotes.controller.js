(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCriar.controllers')
		.controller('lotesCtrl', lotesCtrl);

	lotesCtrl.$inject = ['requestLotes', '$ionicSlideBoxDelegate', '$ionicModal', '$scope'];

	function lotesCtrl(requestLotes, $ionicSlideBoxDelegate, $ionicModal, $scope) {

		var vm = this;

		vm.regex = /[\\?\\&]v=([^\\?\\&]+)/;
		vm.lotes = requestLotes.data.lotes;
		vm.nomeleilao = requestLotes.data.nomeleilao;


		// console.log(requestLotes.data);

		vm.openInfo         = openInfo;
		vm.closeModal       = closeModal;
		vm.openExternalLink = openExternalLink;

		///////////////////////

		if (window.StatusBar) StatusBar.styleLightContent();

		// info modal
		$ionicModal.fromTemplateUrl('template/infoModal.html', {
			scope: $scope
		}).then(function(modal) {
			vm.modal = modal;
		});

		function openInfo(data) {
			$scope.detail = data;
			vm.modal.show();
		}

		function closeModal() {
			vm.modal.hide();
		}

		function openExternalLink(link) {
			window.open(link, '_system', 'location=no');
		}

	}

})();