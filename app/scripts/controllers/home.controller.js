(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCriar.controllers')
		.controller('homeCtrl', homeCtrl);

	homeCtrl.$inject = ['$ionicSideMenuDelegate'];

	function homeCtrl($ionicSideMenuDelegate) {

		var vm = this;

		vm.openLive = openLive;

		vm.aImages = [	{'src' : 'images/imagem1.jpg' },
						{'src' : 'images/imagem2.jpg' },
						{'src' : 'images/imagem3.jpg' }];


		///////////////////////


		$ionicSideMenuDelegate.canDragContent(false)
		if (window.StatusBar) StatusBar.styleDefault();

		function openLive() {

			navigator.notification.alert(
				'Não estamos ao vivo no momento.',  // message
				null,         	// callback
				'Atenção',    	// title
				'OK'          	// buttonName
			);

			// if (device)
			// 	switch(device.platform){
			// 		case 'iOS':
			// 			window.open('http://streaming50.sitehosting.com.br:1935/casamalli4/casamalli4/playlist.m3u8', '_system', 'location=no'); break;
			// 		case 'Android':
			// 			window.open('rtsp://streaming50.sitehosting.com.br:1935/casamalli4/casamalli4', '_system', 'location=no'); break;
			// 	}
		}

	}

})();
