(function(){

	'use strict';

	/* jshint validthis: true */

	angular.module('AppCriar.controllers')
		.controller('agendaCtrl', agendaCtrl);

	agendaCtrl.$inject = ['$rootScope', 'requestAgenda', '$ionicSlideBoxDelegate'];

	function agendaCtrl($rootScope, requestAgenda, $ionicSlideBoxDelegate) {

		var vm = this;

		vm.agendas = requestAgenda.data;

		///////////////////////

		if (window.StatusBar) StatusBar.styleLightContent();

		// intercept when there is no 'lotes'
		$rootScope.$on('$stateChangeError', function(next, current) {
			navigator.notification.alert(
				'Nenhum lote cadastrado.',  // message
				null,         	// callback
				'Atenção',    	// title
				'OK'          	// buttonName
			);
		});
	}

})();