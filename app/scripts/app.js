(function(){

    'use strict';

    // app: Criar Leilões
    // developer by: Douglas de Oliveira

    /* jshint validthis: true */

    angular.module('AppCriar.filters', []);
    angular.module('AppCriar.services', []);
    angular.module('AppCriar.directives', []);
    angular.module('AppCriar.controllers', []);

    angular.module('AppCriar', ['ionic', 'ui.utils.masks', 'AppCriar.httpInterceptor', 'AppCriar.filters', 'AppCriar.services', 'AppCriar.directives', 'AppCriar.controllers'])

    .run(function($ionicPlatform) {
        $ionicPlatform.ready(function() {

            if (typeof(navigator.notification) == "undefined") {
              navigator.notification = window;
            }
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });
    })

    .config(function($stateProvider, $urlRouterProvider, $httpProvider) {

        $stateProvider
          .state('app', {
              url: '/app',
              abstract: true,
              templateUrl: 'templates/menu.html',
              controller: 'menuCtrl as vm'
          })
          .state('app.home', {
              url: '/home',
              views: {
                'menuContent': {
                  templateUrl: 'templates/home.html',
                  controller: 'homeCtrl as vm'
                }
              }
          })
          .state('app.sobre', {
              url: '/sobre',
              views: {
                'menuContent': {
                  templateUrl: 'templates/sobre.html',
                  controller: 'sobreCtrl as vm',
                  resolve: {
                    requestSobre: function($api) {
                      return $api.getSobre();
                    }
                  }
                }
              }
          })
          .state('app.agenda', {
              url: '/agenda',
              views: {
                'menuContent': {
                  templateUrl: 'templates/agenda.html',
                  controller: 'agendaCtrl as vm',
                  resolve: {
                    requestAgenda: function($api) {
                      return $api.getAgenda();
                    }
                  }
                }
              }
          })
          .state('app.lotes', {
              url: '/lotes/:agendaID',
              views: {
                'menuContent': {
                  templateUrl: 'templates/lotes.html',
                  controller: 'lotesCtrl as vm',
                  resolve: {
                    requestLotes: function($api, $stateParams) {
                      return $api.getLotes($stateParams.agendaID);
                    }
                  }
                }
              }
          })
          .state('app.contato', {
              url: '/contato',
              views: {
                'menuContent': {
                  templateUrl: 'templates/contato.html',
                  controller: 'contatoCtrl as vm'
                }
              }
          })
          .state('app.cadastro', {
              url: '/cadastro',
              views: {
                'menuContent': {
                  templateUrl: 'templates/cadastro.html',
                  controller: 'cadastroCtrl as vm'
                }
              }
          });

        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/app/home');
        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('httpInterceptor');
    });

})();
